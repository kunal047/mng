$(document).ready(function () {
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll > 100) {
			$('.navbar').removeClass('navbar-dark').addClass('navbar-light');
			$('.navbar .navbar-brand img').attr('src', 'img/fav.png');
		}

		else {
			$('.navbar').removeClass('navbar-light').addClass('navbar-dark');
			$('.navbar .navbar-brand img').attr('src', 'img/logo.png');
		}
	})
})

$(".navbar a").on("click", function () {
	$(".navbar").find(".active").removeClass("active");
	$(this).parent().addClass("active");
});