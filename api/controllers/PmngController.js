/**
 * Pmng
 *
 * @description :: Server-side logic for managing Pmng
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var fs          = require('fs-extra');
var nodemailer  = require('nodemailer');
var PythonShell = require('python-shell');
var request     = require('request');





module.exports = {

    editProfile: async(req, res, next) => {
        Pmng.findOne( { 'order_id' : req.param('id') }, function Founded(err, value) {
            if (err) {
                return next(err);
            }
            console.log(value);
            return res.view('pages/edit', {
              profilesFilledBy: ["Self", "Parent", "Sibling", "Relative"],
              manglikList: ["Yes", "No", "Do Not Know"],
              gotraList: ["Do Not Know", 'Aachitransh', 'Achitrans', 'Amrans', 'Attlaans', 'Balans', 'Balansh', 'Bhansali', 'Bhattayans', 'Bhatyas', 'Bugdalimb', 'Chandrans', 'Chudans', 'Dhanans', 'Dhumrans', 'Fafdans', 'Gajans', 'Gataumasya', 'Gaurans', 'Gowans', 'Haridrans', 'Jaislani', 'Jhumrans', 'Kagans', 'Kagayans', 'Kamlas', 'Kapilans', 'Karwans', 'Kaschyap', 'Kaushik', 'Khalans', 'Khalansi', 'Liyans', 'Loras', 'Manans', 'Manmans', 'Mugans', 'Musayas', 'Nanans', 'Nanased', 'Nandans', 'Panchans', 'Paras', 'Peeplan', 'Rajhans', 'Sandans', 'Sandhans', 'Sasans', 'Seelans', 'Sesans', 'Silansh', 'Sirses', 'Sodhani', 'Thobdans', 'Vachans', 'Other'],
              heightList: ['4 feet 6 inches', '4 feet 7 inches', '4 feet 8 inches', '4 feet 9 inches', '4 feet 10 inches', '4 feet 11 inches', '5 feet', '5 feet 1 inch', '5 feet 2 inches', '5 feet 3 inches', '5 feet 4 inches', '5 feet 5 inches', '5 feet 6 inches', '5 feet 7 inches', '5 feet 8 inches', '5 feet 9 inches', '5 feet 10 inches', '5 feet 11 inches', '6 feet', '6 feet 1 inch', '6 feet 2 inches', '6 feet 3 inches', '6 feet 4 inches', '6 feet 5 inches', '6 feet 6 inches'],
              maritalList: ['Unmarried', 'Divorced', 'Widow / Widower'],
              physicalStatusList: ['Normal', 'Specially Abled'],
              complexionList: ["Dark", "Wheatish", "Fair"],
              educationCategories: ['BArch/MArch', 'BCom/BSc/BCA/BA/BMS/BMM/Hotel Mgmt', 'BE/BTech', 'CA/CFA/CS/ICWA', 'Diploma', 'Fashion Designing / Home science/ Jewellery designing', 'IAS/IPS/IRS', 'LLB/LLM', 'MA/MCom/MSc/MCA', 'MBA/PGDM', 'MBBS/BAMS/BHMS/BDS', 'MD/MS/MDS/DNB ', 'ME/Mtech/MS', 'PHD', 'Under graduate', 'Other'],
              incomeList: ['Upto Rs. 5 lakh', 'Rs. 5 lakh - Rs. 10 lakh', 'Rs. 10 lakh - Rs. 18 lakh', 'Rs. 18 lakh - Rs. 25 lakh', 'Rs. 25 lakh - Rs. 40 lakh', 'Rs. 40 lakh - Rs. 60 lakh', 'Rs. 60 lakh - Rs. 1 Crore', 'Above Rs. 1 Crore', 'Not Applicable'],
              designationList: ['Employee', 'Director', 'Founder/Proprietor/Owner', 'Co-founder/Partner', 'Professional/Freelancer', 'Studying'],
              familyStatus: ['Middle Class', 'Upper Middle Class', 'Affluent Class'],
              occupationList: ['Business', 'Job', 'Self-Employed', 'Studying'],

              fStatus: ['Middle Class', 'Upper Middle Class', 'Affluent Class'],
              noOfSiblings: ['0', '1', '2', '3', '4', '5'],
              cameToKnow: ['Whatsapp', 'Facebook', 'SMS', 'Email', 'Community Magazine', 'Relative/Friend', 'Other'],
              user: value
            
            });

            
        });

    },

    myProfile: (req, res, next) => {
        console.log(req.param('id'));
        Pmng.findOne( { 'order_id' : req.param('id') }, function Founded(err, value) {
            if (err) {
                return next(err);
            }
            return res.view('pages/myprofile', { user: value });
            
        });
    },

    fillit: (req, res, next) => {
        return res.view('pages/register', 
            { 
            profilesFilledBy : ["Self", "Parent", "Sibling", "Relative"],
            manglikList : ["Yes", "No", "Do Not Know"],
            gotraList : ["Do Not Know", 'Aachitransh', 'Achitrans', 'Amrans', 'Attlaans', 'Balans', 'Balansh', 'Bhansali', 'Bhattayans', 'Bhatyas', 'Bugdalimb', 'Chandrans', 'Chudans', 'Dhanans', 'Dhumrans', 'Fafdans', 'Gajans', 'Gataumasya', 'Gaurans', 'Gowans', 'Haridrans', 'Jaislani', 'Jhumrans', 'Kagans', 'Kagayans', 'Kamlas', 'Kapilans', 'Karwans', 'Kaschyap', 'Kaushik', 'Khalans', 'Khalansi', 'Liyans', 'Loras', 'Manans', 'Manmans', 'Mugans', 'Musayas', 'Nanans', 'Nanased', 'Nandans', 'Panchans', 'Paras', 'Peeplan', 'Rajhans', 'Sandans', 'Sandhans', 'Sasans', 'Seelans', 'Sesans', 'Silansh', 'Sirses', 'Sodhani', 'Thobdans', 'Vachans', 'Other'],
            heightList : ['4 feet 6 inches', '4 feet 7 inches', '4 feet 8 inches', '4 feet 9 inches', '4 feet 10 inches', '4 feet 11 inches', '5 feet', '5 feet 1 inch', '5 feet 2 inches', '5 feet 3 inches', '5 feet 4 inches', '5 feet 5 inches', '5 feet 6 inches', '5 feet 7 inches', '5 feet 8 inches', '5 feet 9 inches', '5 feet 10 inches', '5 feet 11 inches', '6 feet', '6 feet 1 inch', '6 feet 2 inches', '6 feet 3 inches', '6 feet 4 inches', '6 feet 5 inches', '6 feet 6 inches'],
            maritalList : ['Unmarried', 'Divorced', 'Widow / Widower'],
            physicalStatusList : ['Normal', 'Specially Abled'],
            complexionList : ["Dark", "Wheatish", "Fair"],
            educationCategories : ['BArch/MArch', 'BCom/BSc/BCA/BA/BMS/BMM/Hotel Mgmt', 'BE/BTech', 'CA/CFA/CS/ICWA', 'Diploma', 'Fashion Designing / Home science/ Jewellery designing', 'IAS/IPS/IRS', 'LLB/LLM', 'MA/MCom/MSc/MCA', 'MBA/PGDM', 'MBBS/BAMS/BHMS/BDS', 'MD/MS/MDS/DNB ', 'ME/Mtech/MS', 'PHD', 'Under graduate', 'Other'],
            incomeList : ['Upto Rs. 5 lakh', 'Rs. 5 lakh - Rs. 10 lakh', 'Rs. 10 lakh - Rs. 18 lakh', 'Rs. 18 lakh - Rs. 25 lakh', 'Rs. 25 lakh - Rs. 40 lakh', 'Rs. 40 lakh - Rs. 60 lakh', 'Rs. 60 lakh - Rs. 1 Crore', 'Above Rs. 1 Crore', 'Not Applicable'],
            designationList : ['Employee', 'Director', 'Founder/Proprietor/Owner', 'Co-founder/Partner', 'Professional/Freelancer', 'Studying'],
            familyStatus : ['Middle Class', 'Upper Middle Class', 'Affluent Class'],
            occupationList : ['Business', 'Job', 'Self-Employed', 'Studying'],

            fStatus : ['Middle Class', 'Upper Middle Class', 'Affluent Class'],
            noOfSiblings : ['0', '1', '2', '3', '4', '5'],
            cameToKnow : ['Whatsapp', 'Facebook', 'SMS', 'Email', 'Community Magazine', 'Relative/Friend', 'Other']
            });
    },

    create: async (req, res, next) => {
        
        var total = await Pmng.count();
        total = total + 1;

        var indianTimeZoneVal = new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Kolkata'
        });
        var contactTime = new Date(indianTimeZoneVal).toLocaleString();

        photo_dir = `/srv/mng/assets/images/P${total}`;
        bio_dir = `/srv/mng/assets/images/P${total}`;
        if (!fs.existsSync(photo_dir)) {
            fs.mkdirSync(photo_dir);
        }
        if (!fs.existsSync(bio_dir)) {
            fs.mkdirSync(bio_dir);
        }
        var p_id = `P${total}`, p_link = "", p_biodata = "";
        
        var userId = req.param('candidateFirstName').toLowerCase().substring(0, 4);
        var dobIt = req.param('dateOfBirth');
        var dateOfBirth = req.param('dateOfBirth').split('-');
        var dob = `${dateOfBirth[2]}-${dateOfBirth[1]}-${dateOfBirth[0]}`;
        var createLogin = await Plogin.create({
            id       : `P${total}`,
            username : `${userId}${dateOfBirth[2]}${dateOfBirth[1]}`,
            password : req.param('parentMobileNo'),
            gender   : req.param('gender')
        }).fetch();

        var orderId = Math.random().toString(36).substr(2, 8);
        // var orderId = Math.round(new Date().valueOf() + Math.random());

        var createdUser = await Pmng.create({
            id: `P${total}`,
            submission_date: contactTime,
            status: "Unconfirmed",
            profile_filled_by: req.param('profileFilledBy'),
            f_name: req.param('candidateFirstName'),
            l_name: req.param('candidateLastName'),
            mode_of_payment: req.param('paymentMode'),
            order_id: orderId,
            link: req.param('photoUpload'),
            photo_link: "",
            personal_biodata: "",
            dob: dobIt,
            tob: req.param('timeOfBirth'),
            gender: req.param('gender'),
            birth_place: req.param('placeOfBirth'),
            gotra: req.param('gotra'),
            manglik: req.param('manglik'),
            marital_status: req.param('marital'),
            physical_status: req.param('physicalStatus'),
            height: req.param('height'),
            weight: req.param('weight'),
            complexion: req.param('complexion'),
            interests: req.param('interest'),
            education_category: req.param('educationCategory'),
            education_category_other: "",
            education_details: req.param('educationDetails'),
            college: req.param('college'),
            occupation: req.param('occupation'),
            company: req.param('companyName'),
            work_location: req.param('workLocation'),
            designation: req.param('designation'),
            annual_income: req.param('annualIncome'),
            father_name: req.param('fatherFirstName'),
            father_last_name: req.param('fatherLastName'),
            mother_name: req.param('motherFirstName'),
            mother_last_name: req.param('motherLastName'),
            family_occupation: req.param('familyOccupation'),
            family_status: req.param('familyStatus'),
            no_of_sisters: req.param('noOfSisters'),
            no_of_brothers: req.param('noOfBrothers'),
            nanihal: req.param('nanihal'),
            nanihal_location: req.param('nanihalLocation'),
            family_address: req.param('address'),
            city: req.param('city'),
            zipcode: req.param('pincode'),
            country: "",
            state_code: "",
            country_code: "",
            mobile_no: req.param('selfMobileNo'),
            parent_mobile_no: req.param('parentMobileNo'),
            alternate_no: req.param('alternateMobileNo'),
            email: req.param('emailId'),
            horoscope_matching: req.param('horoscope'),
            partner_preference: req.param('partnerPreference'),
            came_to_know: req.param('cameToKnow'),
            personal_biodata_optional: ""

        }).fetch();

        photo_dir = `/srv/mng/assets/images/P${total}/photo`;
        bio_dir = `/srv/mng/assets/images/P${total}/biodata`;
        if (!fs.existsSync(photo_dir)) {
            fs.mkdirSync(photo_dir);
        }
        if (!fs.existsSync(bio_dir)) {
            fs.mkdirSync(bio_dir);
        }

        var uploadFile = req.file('photoUpload');
        console.log(uploadFile);

        uploadFile.upload({
            dirname: photo_dir,
            saveAs: function (file, cb) {
                p_link = file.filename;
                
            cb(null, file.filename);
            }
        }, async function onUploadComplete(err, files) {
            //  Files will be uploaded to .tmp/uploads
            await Pmng.update({ id: p_id }).set({ photo_link: p_link });
            if (err) return res.serverError(err);
            //  IF ERROR Return and send 500 error with error

        });
        
        uploadFile = req.file('biodata');
        // console.log(uploadFile);

        uploadFile.upload({
            dirname: bio_dir,
            saveAs: function (file, cb) {
                p_biodata += file.filename;
                cb(null, file.filename);
            }
        }, async function onUploadComplete(err, files) {
            //  Files will be uploaded to .tmp/uploads
            
            await Pmng.update({ id: p_id }).set({ personal_biodata: p_biodata });
            if (err) return res.serverError(err);
            //  IF ERROR Return and send 500 error with error

            // console.log(files);
        });

        // var options = {
        //     mode: 'text',
        //     args: [req.param('parentMobileNo'), `Thank you for registering  at Meet 'N' Greet, 2018 - Pune.\nYou can view your profile at: https://meetngreet18.com/profile/${orderId}`]
        // };

        // PythonShell.run('sendsms.py', options, function (err, results) {
        //     if (err) throw err;
        //     console.log('results: %j', results);
        // });
        
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
            user: 'registrations@marwadishaadi.com',
            pass: '2911@marwadishaadi'
            }
        });

        function doStuff() {
            if ( p_link === '') { //we want it to match
                setTimeout(doStuff, 50); //wait 50 millisecnds then recheck
                return;
            }
            var mailOptions = {
                from: 'registrations@marwadishaadi.com',
                to: req.param('emailId'),
                bcc: "rishi@marwadishaadi.com",
                subject: "Submission received for Meet 'N' Greet 2018 - Pune.",
                html: `<html><head></head><body><table cellspacing="10" width='500'><tr><td colspan="2"> Hello <b>${req.param('candidateFirstName')} ${req.param('candidateLastName')}</b>.<br></br> <br>Your Profile Id is : P${total}</br> <br>We thank you for showing interest in Meet 'N' Greet 2018 - Pune.</br><br>Your registration is received and recorded. However your participation will only be confirmed after successful payment of Rs. 3700/- towards the event.</br><br>If you haven't made the payment yet, below are the 2 payment options:</br><br><b>1. Credit/Debit Card, Netbanking, E-wallets etc</b></br><br>Click to pay now: <a href="https://meetngreet18.com/paynow/${orderId}">Pay Now</a></br><br><b> 2. NEFT/RTGS/IMPS/Cheque/Cash Deposit in the following account: </b></br><br>Name: Marwadi Shaadi Matrimonial Services</br><br>Bank: Axis Bank.</br><br>Branch: Utalsar, Thane</br><br>Account No: 912020018030211</br><br>IFSC: UTIB0000509</br><br>Amount: Rs. 3700/-</br><br></br><br>We shall keep you updated about your participation and other event related information via email/sms on <b>${req.param('emailId')}</b> and <b> ${req.param('parentMobileNo')}</b></br><br></br><br>In case of any queries, feel free to call: Rishi Karwa 98209 23040 / Priyaa Karwa 90528 54184 / Office no: 022 25476831</br><br></br><br>PS: On rare occasion, incase we feel that you may not get profiles matching your criteria on the day of the event or if you don't fall in the preferences set by fellow participants, we may keep your participation on hold and/or refund your full amount after your consent.</br> </td></tr><div class="test"><tr><td colspan="2"><img src="https://res.cloudinary.com/dpethe0uy/image/upload/v1528263324/logo.png" width="450px" height="200px"></td></tr><tr><td> Maheshwari Meet 'N' Greet 2018, Pune </td></tr><tr><td>Profile filled by? </td><td> ${req.param('profileFilledBy')}</td></tr><tr><td>Full Name</td><td>${req.param('candidateFirstName')} ${req.param('candidateLastName')}</td></tr><tr><td>Profile ID</td><td>P${total}</td></tr><tr><td>Photo (Please click on upload icon after selecting image )</td><td><img src="https://meetngreet18.com/images/P${total}/photo/${p_link}" width="300px" height="400px style="display: block" alt="user_photo" title="user_photo"></td></tr><tr><td>Date of Birth</td><td>${dob}</td></tr><tr><td>Time of Birth</td><td>${req.param('timeOfBirth')}</td></tr><tr><td>Gender</td><td>${req.param('gender')}</td></tr><tr><td>Place of Birth</td><td>${req.param('placeOfBirth')}</td></tr><tr><td>Gotra</td><td>${req.param('gotra')}</td></tr><tr><td>Manglik ?</td><td>${req.param('manglik')}</td></tr><tr><td>Marital Status</td><td>${req.param('marital')}</td></tr><tr><td>Physical Status</td><td>${req.param('physicalStatus')}</td></tr><tr><td>Height</td><td>${req.param('height')}</td></tr><tr><td>Weight (Optional)</td><td>${req.param('weight')}</td></tr><tr><td>Complexion</td><td>${req.param('complexion')}</td></tr><tr><td>Interests</td><td>${req.param('interest')}</td></tr><tr><td>Education Category</td><td>${req.param('educationCategory')}</td></tr><tr><td>Education Details</td><td>${req.param('educationDetails')}</td></tr><tr><td>College / Institute / University</td><td>${req.param('college')}</td></tr><tr><td>Occupation</td><td>${req.param('occupation')}</td></tr><tr><td> Company/Firm Name</td><td>${req.param('companyName')}</td></tr><tr><td> Work Location (Current)</td><td>${req.param('workLocation')}</td></tr><tr><td>Designation</td><td>${req.param('designation')}</td></tr><tr><td>Annual Income (Self)</td><td>${req.param('annualIncome')}</td></tr><tr><td> Father's Name</td><td>${req.param('fatherFirstName')}${req.param('fatherLastName')}</td></tr><tr><td>Mother's Name</td><td>${req.param('motherFirstName')}${req.param('motherLastName')}</td></tr><tr><td> Family Occupation</td><td>${req.param('familyOccupation')}</td></tr><tr><td> Family Status</td><td>${req.param('familyStatus')}</td></tr><tr><td> No. of Sisters</td><td>${req.param('noOfSisters')}</td></tr><tr><td>No. of Brothers</td><td>${req.param('noOfBrothers')}</td></tr><tr><td>Nanihal</td><td>${req.param('nanihal')}</td></tr><tr><td>Nanihal Location</td><td>${req.param('nanihalLocation')}</td></tr><tr><td>Permanent Residence</td><td>${req.param('address')}, ${req.param('pincode')}, ${req.param('city')}</td></tr><tr><td>Parent Mobile No.</td><td>${req.param('parentMobileNo')}</td></tr><tr><td>E-mail</td><td>${req.param('emailId')}</td></tr><tr><td>Horoscope Matching Required?</td><td>${req.param('horoscope')}</td></tr><tr><td>How did you come to know about Meet 'N' Greet?</td><td>${req.param('cameToKnow')}</td></tr></div><tr><td colspan="2">You can <a href="https://meetngreet18.com/profile/${orderId}">view your submission easily</a></td></tr></table></body></html>`
            };
// <a href="https://meetngreet18.com/edit/${orderId}">edit this submission</a> and 
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) { 
                    console.log(error); 
                } else {
                    var orderID = `${ Math.round(new Date().valueOf() + Math.random()) }`;
                    var message =  `Thank you for registering  at Meet 'N' Greet, 2018 - Pune.\nYou can view your profile at: https://meetngreet18.com/profile/${orderId}`;
                    request.post(`http://smspanel.marwadishaadi.com/submitsms.jsp?user=Rishi1&key=d808a22243XX&mobile=${req.param('parentMobileNo')}&message=${message}&senderid=MEETNG&accusage=1`, {
                        form: {
                        }
                    },
                    function (error, response, body) {
                        return res.view('pages/payment', { user: createdUser, order_id: orderID });
                    }); 
                }
            });
        }

        doStuff();
    },

    paynow: function (req, res, next) {
        Pmng.findOne( { 'order_id' : req.param('order_id') }, function Founded(err, value) {
            if (err) {
                return next(err);
            }
            console.log(value);
            var orderID = `${ Math.round(new Date().valueOf() + Math.random()) }`
            return res.view('pages/paynow', {user: value, order_id: orderID});
        });
    },

    update: async function(req, res, next) {
        var updatedUser = await Pmng.update( { order_id: req.param('id') } )
            .set({
                profile_filled_by: req.param('profileFilledBy'),
                f_name: req.param('candidateFirstName'),
                l_name: req.param('candidateLastName'),
                mode_of_payment: req.param('paymentMode'),
                dob: req.param('dateOfBirth'),
                tob: req.param('timeOfBirth'),
                gender: req.param('gender'),
                birth_place: req.param('placeOfBirth'),
                gotra: req.param('gotra'),
                manglik: req.param('manglik'),
                marital_status: req.param('marital'),
                physical_status: req.param('physicalStatus'),
                height: req.param('height'),
                weight: req.param('weight'),
                complexion: req.param('complexion'),
                interests: req.param('interest'),
                education_category: req.param('educationCategory'),
                education_details: req.param('educationDetails'),
                college: req.param('college'),
                occupation: req.param('occupation'),
                company: req.param('companyName'),
                work_location: req.param('workLocation'),
                designation: req.param('designation'),
                annual_income: req.param('annualIncome'),
                father_name: req.param('fatherFirstName'),
                father_last_name: req.param('fatherLastName'),
                mother_name: req.param('motherFirstName'),
                mother_last_name: req.param('motherLastName'),
                family_occupation: req.param('familyOccupation'),
                family_status: req.param('familyStatus'),
                no_of_sisters: req.param('noOfSisters'),
                no_of_brothers: req.param('noOfBrothers'),
                nanihal: req.param('nanihal'),
                nanihal_location: req.param('nanihalLocation'),
                family_address: req.param('address'),
                city: req.param('city'),
                zipcode: req.param('pincode'),
                mobile_no: req.param('selfMobileNo'),
                parent_mobile_no: req.param('parentMobileNo'),
                alternate_no: req.param('alternateMobileNo'),
                email: req.param('emailId'),
                horoscope_matching: req.param('horoscope'),
                partner_preference: req.param('partnerPreference'),
                came_to_know: req.param('cameToKnow')

        }).fetch();
        return res.redirect('../profile/' + req.param('id'));
    },

    delete: function(req, res, next) {
        Pmng.destroy(req.param('id'), function Update(err, value) {
            if (err) {
                return next(err);
            }
            return res.redirect('/Pmng');
        });
    },

};