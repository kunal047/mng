var http = require('http'),
    fs = require('fs'),
    qs = require('querystring'),
    request = require('request');

var nodemailer = require('nodemailer');
var PythonShell = require('python-shell');
var request     = require('request');


module.exports = {
    postRes: async function (req, res) {
        var ccavEncResponse = '',
            ccavResponse = '',
            workingKey = 'D3354C2FF129C1E721C3203F05FE5CB9', //Put in the 32-Bit key shared by CCAvenues.
            ccavPOST = '';

        var encryption = req.body.encResp;
        ccavResponse = await sails.helpers.decrypt(encryption, workingKey);

        console.log(ccavResponse);
        pData = ccavResponse.replace(/=/gi, '":"');
        pData = pData.replace(/&/gi, '","');
        pData = '{"' + pData + '"}';
        var pInfo = JSON.parse(pData);


        if (pInfo.order_status === "Success") {


            var emailId = pInfo.merchant_param4;
            var parentMob = pInfo.merchant_param5;

            await Payment.create({
                id: pInfo.merchant_param3,
                f_name: pInfo.merchant_param1,
                l_name: pInfo.merchant_param2,
                order_id: pInfo.order_id,
                transaction_id: pInfo.tracking_id,
                payment_date: pInfo.trans_date,
                payment_mode: pInfo.payment_mode,
                card_name: pInfo.card_name,
                billing_name: pInfo.billing_name,
                billing_address: pInfo.billing_address,
                billing_city: pInfo.billing_city
            });

            await Pmng.update({
                    id: pInfo.merchant_param3
                })
                .set({
                    status: "Confirmed",
                    payment_mode: pInfo.payment_mode
                });

            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'registrations@marwadishaadi.com',
                    pass: '2911@marwadishaadi'
                }
            });

            var mailOptions = {
                from: 'registrations@marwadishaadi.com',
                to: emailId,
                subject: "Payment received for Meet 'N' Greet 2018 - Pune.",
                bcc: "rishi@marwadishaadi.com",
                text: `Hello ${pInfo.merchant_param1} ${pInfo.merchant_param2}.\nGreetings from the Meet 'n' Greet Team,\nThis email is being sent to confirm your registration for the Meet 'n' Greet 3.0 event happening on 19th August, 2018(Sunday) at Conrad Pune. Further details of the event shall be communicated in future.\nThank you for registering with us. We hope to see you there. `
            };
            
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    var message = `Hello ${pInfo.merchant_param1} ${pInfo.merchant_param2}. We have received your payment of Rs. 3700. Your registration has been confirmed. We'll share more details about the event later.`
                    request.post(`http://smspanel.marwadishaadi.com/submitsms.jsp?user=Rishi1&key=d808a22243XX&mobile=${parentMob}&message=${message}&senderid=MEETNG&accusage=1`, {
                        form: {
                        }
                    },
                    function (error, response, body) {
                        return res.view('pages/invoice', {
                            user: pInfo
                        });
                    });
                }
            });
        } else {
             var emailId = pInfo.merchant_param4;
             var parentMob = pInfo.merchant_param5;

             var transporter = nodemailer.createTransport({
                 service: 'gmail',
                 auth: {
                     user: 'registrations@marwadishaadi.com',
                     pass: '2911@marwadishaadi'
                 }
             });

             var mailOptions = {
                 from: 'registrations@marwadishaadi.com',
                 to: emailId,
                 subject: "Payment failed for Meet 'N' Greet 2018 - Pune.",
                 text: `Hello ${pInfo.merchant_param1} ${pInfo.merchant_param2}. We have failed to receive your payment of Rs. 3700.`
             };

             transporter.sendMail(mailOptions, function (error, info) {
                 if (error) {
                     console.log(error);
                 } else {
                    var message = `Hello ${pInfo.merchant_param1} ${pInfo.merchant_param2}. We have failed to receive your payment of Rs. 3700.`
                    request.post(`http://smspanel.marwadishaadi.com/submitsms.jsp?user=Rishi1&key=d808a22243XX&mobile=${parentMob}&message=${message}&senderid=MEETNG&accusage=1`, {
                        form: {
                        }
                    },
                    function (error, response, body) {
                        return res.view('pages/failed');
                        
                    });
                 }
             });
        }
    }
}