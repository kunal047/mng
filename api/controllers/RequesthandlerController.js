/**
 * Payment
 *
 * @description :: Server-side logic for managing Payment
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
// var request = require('request');
var http = require('http'),
    fs = require('fs'),
    request = require('request'),
    qs = require('querystring');

module.exports = {


    postReq: async function (req, res, next) {
        var body = req.body,
            workingKey = 'D3354C2FF129C1E721C3203F05FE5CB9', //Put in the 32-Bit key shared by CCAvenues.
            accessCode = 'AVPG78FF49AO21GPOA'; //Put in the Access Code shared by CCAvenues.

        body = `merchant_id=4703&order_id=${body.order_id}&currency=INR&amount=${body.amount}&redirect_url=${body.redirect_url}&cancel_url=${body.cancel_url}&language=EN&merchant_param1=${body.merchant_param1}&merchant_param2=${body.merchant_param2}&merchant_param3=${body.merchant_param3}&merchant_param4=${body.merchant_param4}&merchant_param5=${body.merchant_param5}`;
        encRequest = await sails.helpers.encrypt(body, workingKey);

        // merchant_id=4703&order_id=fggfdsgfd&currency=INR&amount=1.00&redirect_url=http%3A%2F%2Fwww.marwadishaadi.com%3A5000%2FccavResponseHandler&cancel_url=http%3A%2F%2Fwww.marwadishaadi.com%3A5000%2FccavResponseHandler&language=EN&billing_name=Peter&billing_address=Santacruz&billing_city=Mumbai&billing_state=MH&billing_zip=400054&billing_country=India&billing_tel=9876543210&billing_email=testing%40domain.com&delivery_name=Sam&delivery_address=Vile+Parle&delivery_city=Mumbai&delivery_state=Maharashtra&delivery_zip=400038&delivery_country=India&delivery_tel=0123456789&merchant_param1=additional+Info.&merchant_param2=additional+Info.&merchant_param3=additional+Info.&merchant_param4=additional+Info.&merchant_param5=additional+Info.&promo_code=&customer_identifier=

        return res.send(`<form id="nonseamless" method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"/> <input type="hidden" id="encRequest" name="encRequest" value="${encRequest}"><input type="hidden" name="access_code" id="access_code" value="${accessCode}"><script language="javascript">document.redirect.submit();</script></form>`);

    }
}