module.exports = {
        attributes: {
            id: {
                type: 'string',
                columnName: 'profile_id',
                required: true
            }, 
            f_name: {
                    type: "string"
            },
            l_name: {
                type: "string"
            },
            order_id: {
                type: "string"
            },
            transaction_id: {
                type: "string"
            },
            payment_date: {
                type: "string"
            },
            payment_mode: {
                type: "string"
            },
            card_name: {
                type: "string"
            },
            billing_name: {
                type: "string"
            },
            billing_address: {
                type: "string"
            },
            billing_city: {
                type: "string"
            }
        }
    }