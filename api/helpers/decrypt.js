var crypto = require('crypto');

module.exports = {

    friendlyName: 'Decrypt message',


    description: 'Returns decrypted message',


    inputs: {

        encText: {
            type: 'string',
            required: true
        },
        workingKey: {
            type: 'string',
            required: true
        }

    },


    fn: async function (inputs, exits) {
        // var result = `Hello, ${inputs.name}!`;
        // return exits.success(result);

        var m = crypto.createHash('md5');
        m.update(inputs.workingKey);
        var key = m.digest();
        var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';
        var decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);
        var decoded = decipher.update(inputs.encText, 'hex', 'utf8');
        decoded += decipher.final('utf8');
        console.log(decoded);
        return exits.success(decoded);
    }

};