var crypto = require('crypto');

module.exports = {

    friendlyName: 'Encrypt message',


    description: 'Returns encrypted message',


    inputs: {

        plainText: {
            type: 'string',
            required: true
        },
        workingKey: {
            type: 'string',
            required: true
        }

    },


    fn: async function (inputs, exits) {
        // var result = `Hello, ${inputs.name}!`;
        // return exits.success(result);

        var m = crypto.createHash('md5');
        m.update(inputs.workingKey);
        var key = m.digest();
        var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';
        console.log("key is " + key);
        var cipher = crypto.createCipheriv('aes-128-cbc', key, iv);
        var encoded = cipher.update(inputs.plainText, 'utf8', 'hex');
        encoded += cipher.final('hex');
        return exits.success(encoded);
    }

};