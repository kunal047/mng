/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */



module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'pages/index'
  },
  'GET /admin/89pVxMnk': 'AdminController.index',
  'GET /edit/:id' : 'PmngController.editProfile',
  'GET /dashboard/:gender' : 'PmngController.dashboard',
  'GET /profile/:id' : 'PmngController.myProfile',
  'GET /register' : 'PmngController.fillit', 
  'GET /fmng' : 'PmngController.index',
  'GET /fmng/show/:id' : 'PmngController.show',
  'GET /fmng/edit/:id': 'PmngController.edit',

  'GET /payment': 'PmngController.payment',

  'POST /create' : 'PmngController.create',
  'POST /update/:id' : 'PmngController.update',
  'GET /paynow/:order_id' : 'PmngController.paynow',
  'POST /ccavRequestHandler' : 'RequesthandlerController.postReq',
  'POST /ccavResponseHandler': 'ResponsehandlerController.postRes',

  'GET /prereg' : 'PloginController.prereg'

// app.post('/ccavRequestHandler', function (request, response) {
//   ccavReqHandler.postReq(request, response);
// });


// app.post('/ccavResponseHandler', function (request, response) {
//   ccavResHandler.postRes(request, response);
//  });
  


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝



  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝

};
